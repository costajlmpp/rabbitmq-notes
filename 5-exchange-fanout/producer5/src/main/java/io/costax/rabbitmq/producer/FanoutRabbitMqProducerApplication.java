package io.costax.rabbitmq.producer;

import io.costax.rabbitmq.producer.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.time.LocalDate;
import java.util.stream.IntStream;

@SpringBootApplication
@EnableScheduling
public class FanoutRabbitMqProducerApplication implements CommandLineRunner {

    // Note: we need to create the queues, exchange and the bindings manually the rabbitMQ server
    // check the About.md file

    @Autowired
    HumanResourceSystemProducer producer;

    public static void main(String[] args) {
        SpringApplication.run(FanoutRabbitMqProducerApplication.class, args);
    }

    @Override
    public void run(final String... args) throws Exception {

        IntStream.range(1, 32)
                .mapToObj(i -> new Employee("emp-" + i, "Employee " + i, LocalDate.now()))
                .forEach(employee -> {

                    producer.sendMessage(employee);

                });

    }
}
