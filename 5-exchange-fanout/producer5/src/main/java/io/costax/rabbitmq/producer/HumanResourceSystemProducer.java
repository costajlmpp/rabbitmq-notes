package io.costax.rabbitmq.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.costax.rabbitmq.producer.entity.Employee;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UncheckedIOException;

@Service
public class HumanResourceSystemProducer {

    private static final String X_HR = "x.hr";

    @Autowired
    private RabbitTemplate rabbitTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    public void sendMessage(Employee emp) {
        var json = toJson(emp);

        // No need to use routing key, it can an empty string.
        // Actually, you can give it any string since fanout will broadcast message to all queues
        // it does not have effect
        rabbitTemplate.convertAndSend(X_HR, "", json);
    }

    private String toJson(final Employee emp) {
        try {
            return objectMapper.writeValueAsString(emp);
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }
}
