package io.costax.rabbitmq.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.costax.rabbitmq.consumer.entity.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.UncheckedIOException;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class MarketingSystemEmployeeConsumer {

    public static final String Q_HR_MARKETING = "q.hr.marketing";

    private static final Logger LOGGER = LoggerFactory.getLogger(MarketingSystemEmployeeConsumer.class);

    private ObjectMapper objectMapper = new ObjectMapper();

    @RabbitListener(queues = Q_HR_MARKETING)
    public void listen(String message) {
        Employee emp = toEmp(message);

        LOGGER.info("Marketing System Employee Employee is {}", emp);

        process(emp);
    }

    private Employee toEmp(final String message) {
        try {
            return objectMapper.readValue(message, Employee.class);
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void process(final Employee emp) {

        try {
            Thread.sleep(ThreadLocalRandom.current().nextLong(10_000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
