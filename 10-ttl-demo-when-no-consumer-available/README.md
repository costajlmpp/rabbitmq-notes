# TTL Demo (When No Consumer Available)

Let’s see an example about `time to live` and `dead letter exchange`.

1. Delete all the resource from the previous examples.

2. Create new fanout exchange `x.mypicture`, this is the exchange for main business process.

3. Create another fanout exchange `x.mypicture.dlx` as dead letter exchange.

4. Next, add queue `q.mypicture.image`.

5. Next, add queue `q.mypicture.image-ttl`. On this queue we need to add;
 
  - The message `TTL` to `5000`. That means if no consumer process the message after 5000 milliseconds, the message will be automatically thrown to dead letter exchange.
  - The `dead letter exchange` for `q.mypicture.image-ttl` to `x.mypicture.dlx`

6. Also create queue `q.mypicture.dlx`, this is queue for `dead letter` now, bind `x.mypicture` into `q.mypicture.image` and `q.mypicture.image-ttl`.

7. Bind `x.mypicture.dlx` into `q.mypicture.dlx`.



Now, let’s run MyPictureProducer from previous lecture, without running consumer.
At this point, we will publish one message to two queues : `q.mypicture.image` and `q.mypicture.image-ttl`
Wait for at least five seconds after producer started to see the difference.

See the Result:

Since we using fanout exchange, there are two messages.
Since consumer application is not running, no consumer for them.
However, only one message that go to dead letter exchange.
That is the message from q.mypicture.image-ttl, where we configured it using time to live to 5000 milliseconds.
Message in q.mypicture.image will stays in queue instead of dead letter exchange.
