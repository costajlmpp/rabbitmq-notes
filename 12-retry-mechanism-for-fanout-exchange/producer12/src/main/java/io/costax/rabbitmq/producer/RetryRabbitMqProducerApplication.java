package io.costax.rabbitmq.producer;

import io.costax.rabbitmq.producer.entity.Employee;
import io.costax.rabbitmq.producer.entity.Picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.time.LocalDate;
import java.util.stream.IntStream;

import static io.costax.rabbitmq.producer.entity.Picture.*;

@SpringBootApplication
@EnableScheduling
public class RetryRabbitMqProducerApplication implements CommandLineRunner {

    public static final String MOBILE = "mobile";
    public static final String WEB = "web";

    private static final long DELIMITER_SIZE = 4_000L;
    private static final long SMALL_SIZE = DELIMITER_SIZE - 1L;
    private static final long LARGE_SIZE = DELIMITER_SIZE + 1L;
    private static final long INVALID_SIZE = 6_500L;


    // Note: we need to create the queues, exchange and the bindings manually the rabbitMQ server
    // check the About.md file

    //@Autowired
    //PictureInputSystemProducer producer;

    @Autowired
    RetryEmployeeProducer producer;

    public static void main(String[] args) {
        SpringApplication.run(RetryRabbitMqProducerApplication.class, args);
    }

    @Override
    public void run(final String... args) {

//        producer.sendMessage(Picture.of(1, PNG, MOBILE, SMALL_SIZE));
//        producer.sendMessage(Picture.of(2, JPG, MOBILE, SMALL_SIZE));
//        producer.sendMessage(Picture.of(3, SVG, MOBILE, SMALL_SIZE));
//
//        producer.sendMessage(Picture.of(4, PNG, MOBILE, INVALID_SIZE));
//        producer.sendMessage(Picture.of(5, JPG, MOBILE, INVALID_SIZE));
//        producer.sendMessage(Picture.of(6, SVG, MOBILE, INVALID_SIZE));


        IntStream.range(1, 2)
                .mapToObj(i -> new Employee("emp-" + i, "Employee " + i, LocalDate.now()))
                .forEach(employee -> {

                    producer.sendMessage(employee);

                });

        producer.sendMessage(new Employee("emp-without-name", "", LocalDate.now()));
    }

}
