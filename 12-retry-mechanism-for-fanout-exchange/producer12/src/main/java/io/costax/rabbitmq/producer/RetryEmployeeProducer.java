package io.costax.rabbitmq.producer;

import io.costax.rabbitmq.producer.entity.Employee;
import io.costax.rabbitmq.producer.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RetryEmployeeProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(RetryEmployeeProducer.class);
    public static final String X_GUIDELINE_2_WORK = "x.guideline2.work";

    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private JsonMapper objectMapper;

    public void sendMessage(Employee emp)  {
        var json = objectMapper.toJson(emp);

        LOGGER.info("Sending to [{}] Exchange, the message [{}]", X_GUIDELINE_2_WORK, json);

        rabbitTemplate.convertAndSend(X_GUIDELINE_2_WORK, "", json);
    }

}
