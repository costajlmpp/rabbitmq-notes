package io.costax.rabbitmq.consumer.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.costax.rabbitmq.consumer.json.CustomLocalDateDeserializer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EmployeeData {

    @JsonProperty("employee_id")
    private String employeeId;

    private String name;

    @JsonProperty("birth_date")
    //@JsonSerialize(using = CustomLocalDateSerializer.class)
    //@JsonDeserialize(using = CustomLocalDateDeserializer.class)
    private String birthDate;

    @Deprecated
    public EmployeeData() {
    }

}
