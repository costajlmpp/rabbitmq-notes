package io.costax.rabbitmq.consumer.rabbitmq;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Represents RabbitMQ Header, part x-death. Tested on RabbitMQ 3.7.x.
 */

@Data
public class RabbitmqHeaderXDeath {

    private int count;
    private String exchange;
    private String queue;
    private String reason;
    private List<String> routingKeys;
    private Date time;

}
