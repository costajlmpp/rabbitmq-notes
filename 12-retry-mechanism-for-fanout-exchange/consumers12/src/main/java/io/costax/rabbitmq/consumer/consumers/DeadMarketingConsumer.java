package io.costax.rabbitmq.consumer.consumers;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class DeadMarketingConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeadMarketingConsumer.class);

    @RabbitListener(queues = {"q.guideline2.marketing.dead"})
    public void listen(Message message, Channel channel) throws IOException {

        LOGGER.info("[Dead] Send some message to the Marketing administrator [{}]", new String(message.getBody()));

        // Send some message to the administrator


        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
