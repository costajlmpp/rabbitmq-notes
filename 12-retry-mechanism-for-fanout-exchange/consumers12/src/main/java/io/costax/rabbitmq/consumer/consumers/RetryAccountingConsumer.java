package io.costax.rabbitmq.consumer.consumers;

import com.rabbitmq.client.Channel;
import io.costax.rabbitmq.consumer.entity.EmployeeData;
import io.costax.rabbitmq.consumer.json.JsonMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class RetryAccountingConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(RetryAccountingConsumer.class);

    private static final String DEAD_EXCHANGE_NAME = "x.guideline2.dead";
    private static final String ROUTING_KEY_ACCOUNTING = "accounting";

    private final JsonMapper objectMapper;
    private final DlxFanoutProcessingErrorHandler dlxFanoutProcessingErrorHandler;

    public RetryAccountingConsumer(JsonMapper objectMapper) {
        this.objectMapper = objectMapper;
        this.dlxFanoutProcessingErrorHandler = new DlxFanoutProcessingErrorHandler(DEAD_EXCHANGE_NAME, ROUTING_KEY_ACCOUNTING);
    }

    @RabbitListener(queues = "q.guideline2.accounting.work")
    public void listen(Message message, Channel channel) {
        try {
            var emp = objectMapper.convertTo(message.getBody(), EmployeeData.class);

            if (StringUtils.isEmpty(emp.getName())) {
                throw new IllegalArgumentException("Name is empty");
            }

            LOGGER.info("On accounting : {}", emp);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);

        } catch (Exception e) {

            LOGGER.warn("Error processing message : {} : {}", new String(message.getBody()), e.getMessage());

            dlxFanoutProcessingErrorHandler.handleErrorProcessingMessage(message, channel);
        }

    }
}
