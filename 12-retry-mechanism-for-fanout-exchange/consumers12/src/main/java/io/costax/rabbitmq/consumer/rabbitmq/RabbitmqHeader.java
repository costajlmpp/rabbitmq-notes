package io.costax.rabbitmq.consumer.rabbitmq;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class RabbitmqHeader {

    private static final String KEYWORD_QUEUE_WAIT = "wait";

    private List<RabbitmqHeaderXDeath> xDeaths = new ArrayList<>(2);
    private String xFirstDeathExchange = StringUtils.EMPTY;
    private String xFirstDeathQueue = StringUtils.EMPTY;
    private String xFirstDeathReason = StringUtils.EMPTY;

    public RabbitmqHeader(Map<String, Object> headers) {

       if (headers != null) {
           final Optional<Object> xFirstDeathExchange = Optional.ofNullable(headers.get("x-first-death-exchange"));
           final Optional<Object> xFirstDeathQueue = Optional.ofNullable(headers.get("x-first-death-queue"));
           final Optional<Object> xFirstDeathReason = Optional.ofNullable(headers.get("x-first-death-reason"));


           xFirstDeathExchange.ifPresent(s -> this.setxFirstDeathExchange(s.toString()));
           xFirstDeathQueue.ifPresent(s -> this.setxFirstDeathQueue(s.toString()));
           xFirstDeathReason.ifPresent(s -> this.setxFirstDeathReason(s.toString()));

           final List<Map<String, Object>> xDeathHeaders = (List<Map<String, Object>>) headers.get("x-death");

           if (xDeathHeaders != null) {

               for (final Map<String, Object> x : xDeathHeaders) {

                   RabbitmqHeaderXDeath hdrDeath = new RabbitmqHeaderXDeath();
                   var reason = Optional.ofNullable(x.get("reason"));
                   var count = Optional.ofNullable(x.get("count"));
                   var exchange = Optional.ofNullable(x.get("exchange"));
                   var queue = Optional.ofNullable(x.get("queue"));
                   var routingKeys = Optional.ofNullable(x.get("routing-keys"));
                   var time = Optional.ofNullable(x.get("time"));

                   reason.ifPresent(s -> hdrDeath.setReason(s.toString()));
                   count.ifPresent(s -> hdrDeath.setCount(Integer.parseInt(s.toString())));
                   exchange.ifPresent(s -> hdrDeath.setExchange(s.toString()));
                   queue.ifPresent(s -> hdrDeath.setQueue(s.toString()));
                   routingKeys.ifPresent(r -> {
                       var listR = (List<String>) r;
                       hdrDeath.setRoutingKeys(listR);
                   });
                   time.ifPresent(d -> hdrDeath.setTime((Date) d));

                   xDeaths.add(hdrDeath);
               }

           }
       }
    }

    public int getFailedRetryCount() {
        // get from queue "wait"
        for (var xDeath : xDeaths) {
            if (xDeath.getExchange().toLowerCase().endsWith(KEYWORD_QUEUE_WAIT)
                    && xDeath.getQueue().toLowerCase().endsWith(KEYWORD_QUEUE_WAIT)) {
                return xDeath.getCount();
            }
        }

        return 0;
    }

    public void setxDeaths(final List<RabbitmqHeaderXDeath> xDeaths) {
        this.xDeaths = xDeaths;
    }

    public void setxFirstDeathExchange(final String xFirstDeathExchange) {
        this.xFirstDeathExchange = xFirstDeathExchange;
    }

    public void setxFirstDeathQueue(final String xFirstDeathQueue) {
        this.xFirstDeathQueue = xFirstDeathQueue;
    }

    public void setxFirstDeathReason(final String xFirstDeathReason) {
        this.xFirstDeathReason = xFirstDeathReason;
    }
}
