package io.costax.rabbitmq.consumer.consumers;

import com.rabbitmq.client.Channel;
import io.costax.rabbitmq.consumer.entity.EmployeeData;
import io.costax.rabbitmq.consumer.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class RetryMarketingConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(RetryMarketingConsumer.class);

    private final JsonMapper objectMapper;

    public RetryMarketingConsumer(JsonMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @RabbitListener(queues = "q.guideline2.marketing.work")
    public void listen(Message message, Channel channel) throws IOException {
        var e = objectMapper.convertTo(message.getBody(), EmployeeData.class);
        LOGGER.info("On marketing : " + e);
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }

}
