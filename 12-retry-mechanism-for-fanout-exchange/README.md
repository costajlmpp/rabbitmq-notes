# Retry Mechanism For Fanout Exchange

![Retry Mechanism for Fanout Exchange Schema diagram](../schemas-and-imgs/section4/retry-mechanism-for-fanout-exchange.png)

- We will have four exchanges `work`, `wait`, `retry`, and `dead`, but we will only have three queues.
- There are two exchanges that has binding to work queue (the green and purple). Those are `work` exchange and `retry` exchange.


- In retry mechanism for `fanout`, all exchanges other than `work` exchange are `direct` exchange.


- `work` is the main business process.
- We will write a consumer that process from the `work` queue, and trigger retry mechanism if exception happens.


- `wait` is the pair which invalid messages will be wait for some time before sent to `retry` exchange. 
- We will use `time to live` and `dead letter exchange` to achieve this.


- `dead` is the pair which invalid messages has been **retried** for N times, but still with error.


- `retry` is additional exchange for routing invalid message to correct queue, **using routing key**.
- This is because `work` exchange is of `fanout` exchange type.
- That means if we push invalid message from `wait` to `work`, the message will be broadcasted again to all binding queues.
  - That means if you have two queue bindings on `work` : `accounting` and `marketing`, and only `accounting` that errors, when retry mechanism push message from `wait` directly to `work`, `marketing` will get another same messages, so it will be duplicate process on `marketing`.
- So we use `direct` **for retry exchange and route the invalid message only to queue that need re-process**


- `wait` is `dead letter exchange` for `work` queue.
- Any message that rejected by Accounting Consumer, but still have `retry` attempt will go to `wait` queue

- `retry` is dead letter exchange for wait queue.
- To move message from `wait` to `retry`, we will set `time to live` on `wait` queue, and also override routing key programmatically.

- Remember:
  - On fanout, routing key is not required, but during retry, we have to publish message back to correct queue.
  - Dead can only get message from Accounting consumer.
  - For any message that has been retried for N times and still error, Accounting Consumer will publish the message to dead queue.


## About the example schema

Exchanges:

  - `x.guideline2.work`
    - type: `fanout`
    
  - `x.guideline2.wait`
    - type: `direct`
    
  - `x.guideline2.retry`
    - type: `direct`
    
  - `x.guideline2.dead`
    - type: `direct`
    
Queues:

  - `q.guideline2.accounting.dead`
    - x-queue-mode: lazy
    
  - `q.guideline2.accounting.wait`
    - x-dead-letter-exchange: x.guideline2.retry
    - x-dead-letter-routing-key: accounting
    - x-message-ttl: 120000 (2 min)
    
  - `q.guideline2.accounting.work`
    - x-dead-letter-exchange: x.guideline2.wait
    - x-dead-letter-routing-key: accounting
    
    
  - `q.guideline2.marketing.dead`
    -  x-queue-mode: lazy
    
  - `q.guideline2.marketing.wait`
    - x-dead-letter-exchange: x.guideline2.retry
    - x-dead-letter-routing-key: marketing
    - x-message-ttl: 120000 (2 min)
    
  - `q.guideline2.marketing.work`
    - x-dead-letter-exchange: x.guideline2.wait
    - x-dead-letter-routing-key: marketing
    
Bindings:

  - Binding 1:
    - Exchange: x.guideline2.dead
    - To Queue: q.guideline2.accounting.dead
    - Routing key: accounting
  
  - Binding 2:
    - Exchange: x.guideline2.wait
    - To Queue: q.guideline2.accounting.wait
    - Routing key: accounting
    
  - Binding 3:
    - Exchange: x.guideline2.retry
    - To Queue: q.guideline2.accounting.work
    - Routing key: accounting
    
  - Binding 4:
    - Exchange: x.guideline2.work
    - To Queue: q.guideline2.accounting.work
    - Routing key: accounting
  

---

  - Binding 5:
    - Exchange: x.guideline2.dead
    - To Queue: q.guideline2.marketing.dead
    - Routing Key: marketing
    
  - Binding 6:
    - Exchange: x.guideline2.wait
    - To Queue: q.guideline2.marketing.wait
    - Routing Key: marketing
    
  - Binding 7:
    - Exchange: x.guideline2.retry
    - To Queue: q.guideline2.marketing.work
    - Routing Key: marketing

  - Binding 8:
    - Exchange: x.guideline2.work
    - To Queue: q.guideline2.marketing.work
    - Routing Key: marketing
