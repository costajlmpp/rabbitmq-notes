# Rabbit MQ in The Cloud

[Cloudamqp.com](https://www.cloudamqp.com/) is managed service for RabbitMQ in cloud.
Using cloudamqp, we can have various rabbitmq deployment plans, depends on our requirement for performance.

Using cloudamqp paid plans, we also has many additional features, like monitoring and diagnostics.
For trying, we can start with no cost, but we will only get basic features.

1. To start, go to [Cloudamqp.com](https://www.cloudamqp.com/).

2. We need to sign up, or login using google or github account. 

  - As a note, the screen on this course might be different when you access cloudamqp.com, and that is normal. They might have change or improve the interface, but the basic functionalities should be the same When we login, we need to create new instance. 
  - For starting we don’t need to provide any credit card or billing information.
  - However, we can only start with very basic plan.
  - You can see the features for paid plans in cloudamqp.com. Basically there are shared instances -which is not recommended for production or paid plans with higher power. For this example, let’s create free plan.

3. I will give name “Development”. Choose data center to deployment, and create the instance. That’s it Now, we have running rabbitmq instance.
  - Of course, this free instance is not suitable for production purpose, but enough to know the basic.
  - If you want to know the features of cloud rabbitmq, click on the instance name.
  - We already get rabbitmq management plugin enabled by default
  - You will get information about host and credential to access Rabbitmq on cloud And you also see the configuration for RabbitMQ.
  - This configuration is minimal since we are using free plans.
  - The more interesting items are in this menu. We can create alarm for certain events. Like when total message in queue are more than 200, that might indicates the consumer is overwhelmed with message and we need to add more consumer.

4. Using cloudamqp basically not different with using local rabbitmq installation. We just need to point our code into cloudamqp instance.
  - To do this, set the rabbitmq settings on `src/main/resources/application.yml`
  - We need to configure `host`, `virtual host`, `username`, and `password`.
  - We will get value for those items from rabbitmq instance detail on cloud in on details of cloudamqp.com.
  - Of course, the value will be different for each instance, so put value for your own instance.


