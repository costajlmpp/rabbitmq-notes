# 8 Handle Consumer Exception With DLX (solution 1)

##### Before we can run the producer and consumer code example we have to create manually all the need it resources in the rabbitMQ Server:

1. Create new fanout exchange: `x.mypicture.dlx` 

    - This is the dead letter exchange.
    - Remember, dead letter exchange can be any type of exchange.
    - I use fanout for simplicity reason.

2. Create queue `q.mypicture.dlx`

    - This is dead letter queue, to gather message that cannot be processed because size too large that will be your example problem.

3. Now we need to bind `x.mypicture.dlx` into `q.mypicture.dlx`.

4. Create the a new Exchange `x.mypicture` it can of the `fanout` type, or of any other type, i just choice this type to the example for simplify of not have to define the routing key

5. We need to Create or Modify queue `q.mypicture.image` to use dead letter exchange.

    - Queues once created, cannot modified. So we need to delete existing `q.mypicture.image`, and create again using dead letter exchange.
    - Create `q.mypicture.image`.
    - Set the **dead letter exchange** parameter click in the link with that name.
    - Add some parameter: the dead letter exchange to `x.mypicture.dlx`.

    NOTE: By default, rabbitMQ will use original **routing key** from message But in case you need to change the routing key (because dead letter exchange needs routing), you have two choices: 
    
        - 1. add new dead letter routing key to override original one, by clicking this link.
        - Or 
        - 2. you keep the original routing key by do nothing.

    Since our dead letter exchange is fanout, we don’t need routing key, so I did not click it

6. Bind `x.mypicture` and `q.mypicture.image`




#### Now with all the resource created we can see the example code 

The easiest way to avoid infinite loop, and put invalid message to dead letter exchange, is by throwing special class of Exception when there are any error.

- Spring provides special exception class to automatically reject message which is `AmqpRejectAndDontRequeueException` So, instead of IllegalArgumentException, we will throw `AmqpRejectAndDontRequeueException`.

- Now the example consumer throwing AmqpRejectAndDontRequeueException is not **requeueing** the message.

- In RabbitMQ Server, invalid message now go to dead letter exchange, which is `x.mypicture.dlx`, that broadcast it into the queue `q.mypicture.dlx`.

- This is the easiest error handling, In the following example next we will achieve same goal with different way.

NOTE: I will not delete queues and exchange used in the example because I'm going to use it in the next examples.