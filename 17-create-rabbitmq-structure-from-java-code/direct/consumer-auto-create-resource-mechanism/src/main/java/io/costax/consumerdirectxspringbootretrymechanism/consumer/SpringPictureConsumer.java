package io.costax.consumerdirectxspringbootretrymechanism.consumer;

import io.costax.consumerdirectxspringbootretrymechanism.entity.Picture;
import io.costax.consumerdirectxspringbootretrymechanism.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SpringPictureConsumer {

    private static final long INVALID_SIZE = 6_500L;

    @Autowired
    private JsonMapper objectMapper;

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringPictureConsumer.class);

    @RabbitListener(queues = "q.spring.image.work")
    public void listenImage(String message) throws IOException {
        var picture = objectMapper.convertTo(message, Picture.class);
        LOGGER.info("Consuming image : {}", picture);

        if (picture.getSize() >= INVALID_SIZE) {
            throw new IOException("Image " + picture + " size too large : " + picture.getSize());
        }

        LOGGER.info("Creating thumbnail & publishing image : {}", picture);
    }

    @RabbitListener(queues = "q.spring.vector.work")
    public void listenVector(String message) {
        var picture = objectMapper.convertTo(message, Picture.class);

        LOGGER.info("Consuming vector : {}", picture);
        LOGGER.info("Converting to image, creating thumbnail & publishing image : {}", picture);
    }

}
