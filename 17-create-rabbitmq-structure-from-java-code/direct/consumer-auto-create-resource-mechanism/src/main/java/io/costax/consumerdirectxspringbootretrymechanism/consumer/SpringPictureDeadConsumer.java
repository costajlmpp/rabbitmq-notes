package io.costax.consumerdirectxspringbootretrymechanism.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SpringPictureDeadConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringPictureDeadConsumer.class);
    
    public static final String Q_SPRING_IMAGE_DEAD = "q.spring.image.dead";
    public static final String Q_SPRING_VECTOR_DEAD = "q.spring.vector.dead";

    @RabbitListener(queues = Q_SPRING_IMAGE_DEAD)
    public void listenImage(String message) throws IOException {
        LOGGER.info("[DEAD] Image Send some notification for the Administrator : {}", message);
    }

    @RabbitListener(queues = Q_SPRING_VECTOR_DEAD)
    public void listenVector(String message) throws IOException {
        LOGGER.info("[DEAD] vector Send some notification for the Administrator : {}", message);
    }
}
