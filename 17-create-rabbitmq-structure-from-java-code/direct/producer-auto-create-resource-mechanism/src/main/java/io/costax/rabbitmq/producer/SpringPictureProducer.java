package io.costax.rabbitmq.producer;

import io.costax.rabbitmq.producer.entity.Picture;
import io.costax.rabbitmq.producer.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpringPictureProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringPictureProducer.class);

    private static final String X_SPRING_WORK = "x.dummy";

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private JsonMapper jsonMapper;

    public void sendMessage(Picture picture) {

        var json = jsonMapper.toJson(picture);

        LOGGER.info("Sending to Exchange [{}] the message [{}]", X_SPRING_WORK, picture);
        rabbitTemplate.convertAndSend(X_SPRING_WORK, "hello", json);
    }
}
