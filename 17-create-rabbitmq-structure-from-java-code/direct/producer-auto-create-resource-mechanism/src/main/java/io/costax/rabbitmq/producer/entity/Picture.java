package io.costax.rabbitmq.producer.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Picture {

    public static final String JPG = "jpg";
    public static final String PNG = "png";
    public static final String SVG = "svg";

    private int id;
    private String extension;
    private String source;
    private long size;

    @Deprecated
    Picture() {
    }

    private Picture(final int id, final String extension, final String source, final long size) {
        this.id = id;
        this.extension = extension;
        this.source = source;
        this.size = size;
    }

    public static Picture of(int id, String extension, String source, final long size) {
        return new Picture(id, extension, source, size);
    }

}
