# RabbitMQ


## Getting Started With The Examples:

Naming conventions

- All letters are lowercase
- Exchange name is "x.[name]"
- Queue name is "q.[name].[sub-name]"

Note: This conventions is not an official rabbitMQ convention, but it is very used by many teams to avoid miscommunication. 

## Section 1: Start Writing Codes

1. `1-hello-rabbitMQ`

    - Create a manually the queue `examples.queue.hello1`
    - No necessary create any Exchange
    - The Rabbit MQ default Exchange one will be use

2. `2-consume-is-real-time-indeed`

    - Create manually the queue `q.example2.fixedrate`
    - No necessary create any Exchange
    - The Rabbit MQ default Exchange one will be use

3. `3-multiple-consumers-for-each-queue`


---

## Section 2: Working with JSON

**Why JSON?**

RabbitMQ can accept any string, but this flexibility can have side effect. If a producer X publish message using it's own proprietary format, then every consumer X must know the same format.

Consider this scenario:

- Producer X1 publish message with  || as delimiter between field
- Producer X2 publish message with  ; as delimiter between field

If application C needs to consume from X1 and X2, application C need to know two rules. Imagine if there are more than two rules.

At this point, JSON is really helpful. Nowadays, JSON is de-facto-standard for data interchange. Java (and almost any programming language) has library to create and parse JSON string. So the format became standard, and application C can now focus on business logic rather than parsing logic.


1. `4-producing-and-consuming-json-messages`

    - Create the Simple Queue manually: `q.example4.employee`
    - No necessary create any Exchange
    - The Rabbit MQ default Exchange one will be use

---


## Section 3: RabbitMQ Exchange

In messaging system, programmer do not program the message to be sent directly to the specific receivers (subscribers).

Instead, the programmer "publishes" messages, without any knowledge of any subscribes there may be. Similarly, subscribers express interest in one or more events, and only receive messages that are of interest, without any knowledge of any publishers.

RabbitMQ act as a message broker that receives messages. We can call RabbitMQ as "Unique Post Office" based on this analogy. If RabbitMQ is "Unique Post Office", then it has employees whose jobs is distribute incoming message to queue(s).

That "Employee" called as "**Exchange**". Exchange main task is **Distribute message to queues**.

In some exchanges, it also copy message if there are more than one queue related to exchange.

Exchange distribute message based on **`Routing key`**, in message `Routing key` is like the Address in message Envelope.

There are several types of RabbitMQ exchange, each one routes message differently, for examples:

  - Fanout
  - Direct
  - Topic

Correlation between an exchange with queues is called as binding.

Exchange responsibilities:
    
  - Distribute message to queue(s)
  - Copy message if necessary
  - Based on message routing key


### Fanout Exchange

- Multiple queues for single message
- Broadcast to all queues bound to it



Fanout exchange, sometimes called Pub/Sub or Publish/Subscriber, will broadcast message to all queues with binding to it. 

Sample Scenario:

- For example, when there is a data of new employee, or employee resign on Human Resource System, both Accounting System and Marketing System needs to know this data.

- Accounting System and Marketing system then process this data according to their needs.

- In this case, we can have Human Resource System as producer. Accounting System and Marketing System, each is a consumer.

- Human Resource System will send message to fanout exchange. This exchange has two binding, one for the Accounting System, and one for Marketing System.

- Fanout exchange will copy and send employee data to all queues.

Example code: 5-exchange-fanout

![fanout-schema of the example code](schemas-and-imgs/section2/fanout-exanche.png)

1. On RabbitMQ, let's create two queues: `q.hr.accounting` and `q.hr.marketing`.

2. Click on tab "Exchange", and create new exchange, with the name `x.hr` and the type `fanout`, let the other field to default

3. Enter in the exchange `x.hr`, we need to create binding from this exchange to the queues.

4. So, click on bindings, and add two bindings:
    - One for `q.hr.accounting` queue
    - One for `q.hr.marketing` queue
    
5. we will not use routing key for fanout exchange. Fanout exchange will blast message to every queue that has binding to it, so if you have 10 queues bound to fanout exchange, all the 10 queues will get exact published.


### Direct Exchange

**Summary:**
  - Send to selective queue(s)
  - Based on routing key
  - Message can be discarded


If you need to selectively send message to queues instead of broadcast to all queues, you can use `Direct` exchange.

With direct exchange, you write a routing key into message, and the exchange will send message to queue (or queues) based on this `routing key` and routing rule.


**NOTE:** 
Be careful though, **if routing key does not match any rule, the message will be discarded and never sent to RabbitMQ**.

**Consider this scenario:**

You have a system that accept picture as input. The picture can be png, jpg, or svg

- If the picture is png or jpg, you need to create thumbnail for picture, and publish the picture to your company blog.

- However, if the picture is svg -which is vector-, you’ll need to convert it into png before create thumbnail and publish

So basically we have two different process here : the one which involves converting svg vector to image, and the one that does not.

**In this case, we can have two queues, with file type as routing key.**

This is the schema that we will use on this example.

![Direct Exchange Example Schema](schemas-and-imgs/section2/direct-exchange.png)

- We will create direct exchange, with name `x.picture` there will be two **queue bindings** : `q.picture.image` for processing image, and `q.picture.vector` for processing vector `q.picture.image` will handle **routing key** `jpg` and `png`, while `q.picture.vector` will handle routing key svg
- If the routing key is not jpg, png, or svg, but you send the message to RabbitMQ, the message will be discarded.
- So for example if you send a message with gif routing key, the message will not go to any queue and will be gone. This is the default behaviour of rabbitmq.


**Creating Manually the Queues, Exchange and the Bindings in RabbitMQ Server:**

1. On rabbitmq, let’s create two queues: `q.picture.image` and `q.picture.vector`

2. Next, click on tab “Exchange”, and create new exchange. Give the name `x.picture` and type `direct`. Leave the other field with the default values.

2. Click and Enter on `x.picture` Exchange. We need to create binding from this exchange to the queues.  So click on bindings, and add three bindings, one for each routing key.

  - First binding goes to `q.picture.image` with routing key `jpg`
  - Second binding goes to `q.picture.image` with routing key `png`
  - Third binding goes to `q.picture.vector` with routing key `svg`


**@See the code `6-exchange-direct` project**

  - As you can see, consumers consume message with total ten messages.
  - Well done, you just created producer and consumer for direct exchange.


**Extra Notes and Conclusions:**

  - I just want to remind you the benefit of messaging system from this example scenario.
  
  - Let’s assume that process to create thumbnail,publish to blog, and convert vector to image take one second each.
  
  - Without messaging system like rabbitmq, every svg process on client will take time to upload image plus three seconds processing. In other words, the clients needs at least three second time before client can upload another picture.

  - With rabbitmq, client only need time to upload image plus time to publish message to rabbitmq, which surely take a lot less than three seconds.

  - So your client get faster response rather than three seconds wait.

  - Also, if you have a lot of clients which upload at very fast time, you can always scale your rabbitmq consumer machine rather than scaling your web server, which may take less cost.

  - Don’t forget, you can also set multiple concurrent consumers easily on spring for even faster process.


### Topic Exchange

**Summary:**
  - Multiple criteria routing
  - More than routing key, Eg: Picture type & source & size

  - Based on routing key
  - Message can be discarded


- Direct exchange is capable to routing message selectively. **But it can only handle message based on single criteria**, in our previous example it's based on file type: image or vector.
- In Same messaging system we may want to routing not only based on the image type, but also based on the source which produced the picture, for example whether the picture come from web app or mobile app.
- Also we may need to do process based on picture size: small or large.

This is the schema that we will use on this example.

Same from previous example in direct exchange: we still need to process thumbnail, and publishing.
Therefore we need to convert vector to image. 

  - So this is based on picture type
  - Now we have two additional requirements:
    - Image filter for source: mobile
    - Log large-size vector


Think about the problem:

- Without RabbitMQ, you will need to implement those logics on upload handler.
- To speed up process, you will need to optimize codes on upload handler.
- With rabbitMQ, you just need a way to send message to correct queues and let the consumer works asynchronously.


- RabbitMQ has `topic exchange` to routing message with **multiple criteria**.
- In topic exchange, message `routing key` must be list of words, **delimited by dots** ('.').

- The words can be anything, but usually they specify some features related to the message.
- Topic exchange is almost similar as direct exchange.
``
    - A message sent with a particular `routing key` will be delivered to all the queues that are bound with a matching binding rule.

    - However there are two important character for binding:

        - **`*` (star) can substitute for exactly one word.**
        - **`#` (hash) can substitute for zero or more words.**

**Let’s see the implementation for this scenario**

![Topic Exchange Example Diagram](schemas-and-imgs/section2/topic-exchange.png)

This is RabbitMQ schema that we will use on this lecture.


1. On rabbitmq, let’s create all queues: 

    - `q.picture2.image`
    - `q.picture2.vector`
    - `q.picture2.filter`
    - `q.picture2.log`

2. Next, click on tab “Exchange”, and create new exchange. Give the name `x.picture2` and type `topic`. Leave the other field

3. Click on “x.picture2”. We need to create binding from this exchange to the queues.

4. So click on bindings, and add five bindings like on diagram

    - First binding goes to `q.picture2.image` with routing key `*.*.png`

    - Second binding goes to `q.picture2.image` with routing key `#.jpg`

    - Third binding goes to `q.picture2.vector` with routing key `*.*.svg`

    - Fourth binding goes to `q.picture2.filter` with routing key `mobile.#`

    - Last binding goes to `q.picture2.log` with routing key `*.large.svg`


**@See the code `7-exchange-topic` project**


Congratulations, you have written another program, now for topic exchange.


---

## Section 4: Basic Error Handling

This section will talk about **basic error handling** in RabbitMQ.

Message can live forever in a queue, if no consumer that consumes it. Or, message can be discarded and gone if something wrong happened during consumer logic.

RabbitMQ has a mechanism called `DLX`, or `Dead Letter Exchange`. Messages from a queue can be `dead-lettered`; that is, **republished** to another exchange when any of the following events occur: 

  - The message is rejected with requeue=false

  - The TTL (time to live) for the message expires

  - The queue length limit is exceeded

This mechanism is useful to handle invalid messages, without discard it.
In this section, we will learn about Dead Letter Exchange, and TTL (Time To Live), and of course we will do some java coding.


---

So far, in the previous examples, we only process messages without errors. It is programmer's responsibility to write good, reliable code.

However, in real life, sometimes you will find some message that cannot be processed. In our picture example from previous section, maybe the image size is too large, so consumer threw an exception.

**What happened then?**

- In case of exception, Spring will **re-queue** the message for processing later.

**But what happens if the error stays?**

- In case of size too large, no matter how many times consumer try, it will always throws an exception and **re-queue**, creating infinite loop.


Solutions:
To avoid this, we can create `dead letter exchange`, and when the consumer throws exception, **you can “reject” the message and set the requeue flag as false**.

This way, the problematic message will be sent to `dead letter exchange`, which in turn, goes to another queue (depends on how you set the binding to dead letter exchange), and you can have another consumer that process the exception queue.

Maybe sending notification to administrator about the picture info.
Despite it’s scary name, `dead letter exchange` or `DLX` is just normal exchange.
They can be any of the exchange types and are created as usual.

Another case is when the consumer not available.

Imagine that our picture vector consumer, for some reason, is suddenly dead, That means you have several choices: Let the messages stays in vector queue, or Set timeout to vector queue, so after some timeout, the message will be sent to `dead letter exchange`, and in turn to another queue.

Another consumer with binding on this queue can then process the message, maybe send notification to admin.


### TTL or Time to Live

In previous topic about `dead letter exchange`, we mentioned message **timeout**.

Timeout in rabbitMQ is called `TTL` or `time to live`. That is, the **milliseconds** that message can live in a queue, without any consumer consuming it.

A message that has been in the queue for longer than the configured `TTL` is said to be **dead**, and if there is any `dead letter exchange` configured for the queue, that message will be sent to `dead letter exchange`, which in turn, go to another queue (depends on how you set it)

This is handy for various cases, not only sending unprocessed message, but also enable us to create retry mechanism that you will see later.


### Consumer Exception without DLX Solution 1

Using The Spring `AmqpRejectAndDontRequeueException` Exception:

See the [readme.md](8-handle-consumer-exception-with-dlx-solution-1/README.md) and the example code in the [8-handle-consumer-exception-with-dlx-solution-1 project](8-handle-consumer-exception-with-dlx-solution-1).


### Consumer Exception without DLX Solution 2

As the alternative solution, we can do manual rejection if there is an error, instead of throwing Exception.

See the [readme.md](9-handle-consumer-exception-with-dlx-solution-2/README.md) and the example code in the [9-handle-consumer-exception-with-dlx-solution-2 project](9-handle-consumer-exception-with-dlx-solution-2).

### TTL Demo (when-no-consumer-available)

See the [readme.md](10-ttl-demo-when-no-consumer-available/README.md)


### Which DLX Solution to Choose?

We can do automatic rejection by throwing `AmqpRejectAndDontRequeueException`, and manual rejection by using Channel and c`hannel.basicReject()`. Notice that on manual rejection, you have to change configuration on Spring's `application.properties`.

This configuration works for all consumers within your program. So if you're using manual rejection, you must also manually acknowledge processed message using `channel.basicAck()` for all consumers. Otherwise, the message will keep processed everytime consumer restarts.

At this point (with `application.properties` modified AND we didn't do any manual acknowledge on PictureXxxConsumer), if you publish a message into q.picture.xxx, and PictureXxxConsumer process it, the message will thrown back to queue as unacknowledged. When the consumer restarts, PictureXxxConsumer will re-process this message.

So choose only one approach for DLX error handling: let Spring handles acknowledge, and throw AmqpRejectAndRequeueException OR, do manual rejection

So, Why Manual Reject?

Manual reject is risky. If we forget to manually acknowledge processed message, there's a possibility it will be re-processed.
Manual reject is basically not required. The benefit of using Spring is that Spring handles this kind of low-level, repetitive task for you.

---

## Section 4: Error Handling with Retry Mechanism

For this section, you need to understand things that we have learned on previous sections, including: 
    
  - dead letter exchange
  - time to live
  - manual acknowledge & reject

We also need Postman (see Speed Up Rabbit MQ Structure below).

Speed Up RabbitMQ Structure

We need to create several exchanges and queues, and combine them using bindings and dead letter exchange. 
This process is quite long and error-prone if done manually from RabbitMQ management console. 
For this reason, we will use RabbitMQ REST API to create the RabbitMQ structure.

I will provide API collection, where you can change some variable and generate RabbitMQ structure automatically. To use this API collection, please install Postman.

For postman collection, you can download it from lecture's resource.

### Why Retry Mechanism?

- Either keep, or send to DLX
- Need something in between
- Retry after x seconds, for N times
- After more than N, send to DLX


### RabbitMQ REST API

- Manual setting is show and very very error-prone
- Used by many teams to configure the RabbitMQ schemas to use
- Contains a Postman collections for easier setup

### RabbitMQ Schema

Retry Mechanism for Direct Exchange

11-retry-mechanism-for-direct-exchange

We will have three pairs of `exchange` and `queue` : `work`, `wait`, and `dead`