# Section 4: Retry Mechanism for Direct Exchange

### RabbitMQ Schema

![Retry Mechanism for Direct Exchange rabbitMQ Schema](../schemas-and-imgs/section4/retry-mechanism%20for-direct-exchange.png)


To create all the schema this time we can use the [Postman collections file](RabbitMQ%20-%20Direct%20Retry.postman_collection.json)

1. Import the collection to Postman 
2. The HTTP requests are in order, so, we can run then one by one or run the the entire collection using the Postman collection Runner option.
    - All variables are with default values, so it is not necessary to make any changes, If you use an environment other than the default, you may need to change some variable.

#### Description:

- We will have three pairs of exchange and queue: `work`, `wait`, and `dead`.

- `work` Pair 

  - **Work** is the main business process.
  - We will write a consumer that process from this queue, and trigger retry mechanism if exception happens.
  - We must define queue of this pair with properties:
    - `x-dead-letter-exchange` = `x.guideline.wait`

- `wait` Pair

  - **Wait** is the pair which **invalid messages** will be wait for some time before sent back to `work exchange`.
  - We will use `time to live` and `dead letter exchange` to achieve this.
  - `Wait` is `dead letter exchange` for work queue.
  - We must define queue of this pair with the properties:
     - `x-dead-letter-exchange` = `x.guideline.work` because we want to requeue the messages for the original `work` queue
     - `x-message-ttl` = `30000` (30 seconds)
  

- `dead` Pair

  - **Dead** is the pair which invalid messages has been retried for N times, but still error.
  - The message to the Dead exchange must be moved manually, see the class [DlxProcessingErrorHandler](consumers11/src/main/java/io/costax/rabbitmq/consumer/rabbitmq/DlxProcessingErrorHandler.java)
  


- Any message that rejected by **Image Consumer**, but still have retry attempt will go to `wait` queue
- `work` is dead letter exchange for `wait` queue.
- To move message from `wait` to `work`, we will set `time to live` on `wait` `queue` 


### In Example code side:

- we will have **1** `publisher` and "2" `consumers`.

- Consumer 1:

  - First consumer will consumes from `work` `queue`.
  - This consumer will reject message that is not processable.
  - The retry mechanism will be handled by `wait` and `work` pairs, using `dead letter exchange` and time to live.

  - For message that has been retried for several times, but still not processable, image consumer will re-publish it to dead pair. So, dead pair can only get message from Image consumer

- Consumer 2:

  - Second consumer is used to consume from `dead queue.` 
  - This consumer can do different business process.
  - For example, **logging the unprocessable messages** or **send notification to administrator**.

---

Consumer with Retry Mechanism

1. Check retry limit
2. if retry limit < threshold, send message to wait exchange.
3. If retry limit reached, send message to dead exchange by re-publish it.