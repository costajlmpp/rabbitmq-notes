package io.costax.rabbitmq.producer;

import io.costax.rabbitmq.producer.entity.Picture;
import io.costax.rabbitmq.producer.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RetryPictureProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(RetryPictureProducer.class);

    private static final String X_GUIDELINE_WORK = "x.guideline.work";

    @Autowired
    JsonMapper jsonMapper;

    @Autowired
    RabbitTemplate rabbitTemplate;

    public void sendMessage(Picture p) {
        var json = jsonMapper.toJson(p);

        LOGGER.info("Sending to Exchange [{}] the message [{}]", X_GUIDELINE_WORK, json);

        rabbitTemplate.convertAndSend(X_GUIDELINE_WORK, p.getExtension(), json);
    }
}
