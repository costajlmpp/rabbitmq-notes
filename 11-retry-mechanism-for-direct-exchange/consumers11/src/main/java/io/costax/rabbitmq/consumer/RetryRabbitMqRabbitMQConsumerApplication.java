package io.costax.rabbitmq.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetryRabbitMqRabbitMQConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetryRabbitMqRabbitMQConsumerApplication.class, args);
    }

}
