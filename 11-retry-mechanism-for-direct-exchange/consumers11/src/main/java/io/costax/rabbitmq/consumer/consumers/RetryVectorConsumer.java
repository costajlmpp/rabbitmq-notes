package io.costax.rabbitmq.consumer.consumers;

import com.rabbitmq.client.Channel;
import io.costax.rabbitmq.consumer.entity.Picture;
import io.costax.rabbitmq.consumer.json.JsonMapper;
import io.costax.rabbitmq.consumer.rabbitmq.DlxProcessingErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class RetryVectorConsumer {

    private static final long INVALID_SIZE = 6_500L;
    private static final String DEAD_EXCHANGE_NAME = "x.guideline.dead";

    private static final Logger log = LoggerFactory.getLogger(RetryVectorConsumer.class);

    private final DlxProcessingErrorHandler dlxProcessingErrorHandler;
    private final JsonMapper objectMapper;

    public RetryVectorConsumer(JsonMapper objectMapper) {
        this.objectMapper = objectMapper;
        this.dlxProcessingErrorHandler = new DlxProcessingErrorHandler(DEAD_EXCHANGE_NAME);
    }

    @RabbitListener(queues = "q.guideline.vector.work")
    public void listen(Message message, Channel channel) {
        try {

            var p = objectMapper.convertTo(message.getBody(), Picture.class);

            processTheImage(p);

            log.info("Convert to image, creating thumbnail, & publishing : " + p);

            // you must acknowledge that message already processed successful
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);

        } catch (IOException e) {

            log.warn("Error processing message : " + new String(message.getBody()) + " : " + e.getMessage());
            dlxProcessingErrorHandler.handleErrorProcessingMessage(message, channel);
        }
    }

    private void processTheImage(final Picture p) throws IOException {
        // process the image
        if (p.getSize() >= INVALID_SIZE) {
            // throw exception, we will use DLX handler for retry mechanism
            throw new IOException("Size too large");
        }
    }
}
