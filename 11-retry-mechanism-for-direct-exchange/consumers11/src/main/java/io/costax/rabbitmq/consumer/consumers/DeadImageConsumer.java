package io.costax.rabbitmq.consumer.consumers;

import com.rabbitmq.client.Channel;
import io.costax.rabbitmq.consumer.entity.Picture;
import io.costax.rabbitmq.consumer.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class DeadImageConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeadImageConsumer.class);

    @Autowired
    JsonMapper jsonMapper;

    @RabbitListener(queues = {"q.guideline.image.dead", "q.guideline.vector.dead"})
    public void listen(Message message, Channel channel) throws IOException {

        final Picture p = jsonMapper.convertTo(message.getBody(), Picture.class);
        LOGGER.info("Send a message to the administrator cause by the message can't be processed [{}]", p);

        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
