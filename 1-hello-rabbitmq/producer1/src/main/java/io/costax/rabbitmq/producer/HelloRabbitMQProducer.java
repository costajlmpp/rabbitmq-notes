package io.costax.rabbitmq.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HelloRabbitMQProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send(String name) {
        rabbitTemplate.convertAndSend("examples.queue.hello1", "hello " + name);
    }
}
