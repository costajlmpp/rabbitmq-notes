package io.costax.rabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class HelloRabbitConsumer {

    @RabbitListener(queues = "examples.queue.hello1")
    public void listen(String message) {
        System.out.println("Consuming: " + message);
    }

}
