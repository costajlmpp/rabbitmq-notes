package io.costax.rabbitmqpublisherapi.api;

import lombok.Getter;

@Getter
public class Result {

    private boolean published;
    private String message;

    private Result(final boolean published, final String message) {
        this.published = published;
        this.message = message;
    }

    public static Result createResult(final boolean published, final String message) {
        return new Result(published, message);
    }
}
