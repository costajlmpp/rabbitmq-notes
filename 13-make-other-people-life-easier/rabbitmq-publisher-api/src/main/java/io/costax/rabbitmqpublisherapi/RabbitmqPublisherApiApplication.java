package io.costax.rabbitmqpublisherapi;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RabbitmqPublisherApiApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqPublisherApiApplication.class, args);
    }

    @Override
    public void run(final String... args) throws Exception {

    }
}
