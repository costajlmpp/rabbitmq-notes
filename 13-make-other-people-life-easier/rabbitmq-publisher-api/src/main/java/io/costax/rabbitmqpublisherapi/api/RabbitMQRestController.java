package io.costax.rabbitmqpublisherapi.api;

import io.costax.rabbitmqpublisherapi.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class RabbitMQRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQRestController.class);

    @Autowired
    private JsonMapper jsonMapper;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping(path = "/api/ping")
    public String ping() {
        return "" + System.currentTimeMillis();
    }


    @PostMapping(path = {
            "/api/publish/{exchange}/{routingKey}",
            "/api/publish/{exchange}"
    }, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Result> publish(@PathVariable(name = "exchange", required = true) String exchange,
                                          @PathVariable(name = "routingKey", required = false) Optional<String> routingKey,
                                          @RequestBody String message) {

        if (!jsonMapper.isValidJson(message)) {

            return ResponseEntity
                    .badRequest()
                    .body(Result.createResult(false, "The json message is not valid to send to the exchange"));
        }

        try {

            rabbitTemplate.convertAndSend(exchange, routingKey.orElse(""), message);

            return ResponseEntity
                    .ok(Result
                            .createResult(true,
                                    String.format("Message published to Exchange [%s] with Routing-key [%s]", exchange,
                                            routingKey.orElse(""))));

        } catch (Exception e) {
            LOGGER.error("Error when publishing : [{}]", e.getMessage());

            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(Result.createResult(false, e.getMessage()));
        }
    }
}
