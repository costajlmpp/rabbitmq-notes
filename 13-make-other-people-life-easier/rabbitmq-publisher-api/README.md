# RabbitMQ Publisher API

The main idea of this little project is to facilitate to publish messages to existing RabbitMQ Server. We can publish messages using the HTTP Request:




```
curl --location --request POST 'localhost:8080/api/publish/{{exchange}}/{{routing-key}}' \
--header 'Content-Type: application/json' \
--data-raw '{
	"id": 123,
	"name": "just a dummy test"
}'
```


## Tips : Spring @Scheduled

Spring @Scheduled can receive several parameters

### Using fixedDelay

`@Scheduled(fixedDelay = 5000)` 

 - Using this parameter, the duration between the end of last execution and the start of next execution is fixed (in milliseconds). The new task always waits until the previous one is completed.
 - So in code above, if the first task needs 20 seconds to finish, the second task will be started at second 25.



### Using fixedRate

`@Scheduled(fixedRate = 5000)` 

- Using this parameter, the task will run every fixed rate interval (in milliseconds). The new task wil not waits until the previous one is completed.
- So in code above, even if the first task needs 20 seconds to finish, the second task will be started at second 5, the third task at second 10.



### Using Spring cron expression

`@Scheduled(cron="0 * * * * *")`

- If we need further flexibility, Spring scheduler comes with Spring cron expression. The detail about Spring cron expression can be seen [here](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/scheduling/support/CronSequenceGenerator.html).