package io.costax.rabbitmq.consumer;

import io.costax.rabbitmq.consumer.entity.Picture;
import io.costax.rabbitmq.consumer.json.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OtherProcessor {

    public static final String Q_PICTURE_OTHER = "q.picture.other";

    private static final Logger LOGGER = LoggerFactory.getLogger(OtherProcessor.class);

    @Autowired
    Json json;

    @RabbitListener(queues = Q_PICTURE_OTHER)
    public void listen(String message) {
        Picture obj = json.convertTo(message, Picture.class);

        LOGGER.info("***> Other Processor System processing is {}", obj);
    }
}
