package io.costax.rabbitmq.consumer.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.UncheckedIOException;

@Component
public class Json {

    private ObjectMapper objectMapper = new ObjectMapper();

    public <T> T convertTo(final String json, Class<T> classType) {
        try {
            return objectMapper.readValue(json, classType);
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }
}
