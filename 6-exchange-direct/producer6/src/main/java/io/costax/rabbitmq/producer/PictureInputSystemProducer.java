package io.costax.rabbitmq.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.costax.rabbitmq.producer.entity.Picture;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UncheckedIOException;

@Service
public class PictureInputSystemProducer {

    private static final String X_PICTURE = "x.picture";

    @Autowired
    private RabbitTemplate rabbitTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    public void sendMessage(Picture picture) {
        var json = toJson(picture);

        rabbitTemplate.convertAndSend(X_PICTURE, picture.getExtension(), json);
    }

    private String toJson(final Picture picture) {
        try {
            return objectMapper.writeValueAsString(picture);
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }
}
