package io.costax.rabbitmq.producer;

import io.costax.rabbitmq.producer.entity.Picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.stream.IntStream;

@SpringBootApplication
@EnableScheduling
public class DirectRabbitMqProducerApplication implements CommandLineRunner {

    // Note: we need to create the queues, exchange and the bindings manually the rabbitMQ server
    // check the About.md file

    private static final String JPG = "jpg";
    private static final String PNG = "png";
    private static final String SVG = "svg";

    @Autowired
    PictureInputSystemProducer producer;

    public static void main(String[] args) {
        SpringApplication.run(DirectRabbitMqProducerApplication.class, args);
    }

    @Override
    public void run(final String... args) {
        IntStream.range(1, 101)
                .mapToObj(i -> Picture.createPicture(i, toExtension(i)))
                .forEach(producer::sendMessage);
    }

    static String toExtension(int index) {
        //if (true) return JPG;

        if (index % 5 == 0) {
            return PNG;
        }

        if (index % 2 == 0) {
            return SVG;
        }

        return JPG;
    }
}
