package io.costax.rabbitmq.producer.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Picture {

    private int id;
    private String extension;

    @Deprecated
    Picture() {
    }

    private Picture(final int id, final String extension) {
        this.id = id;
        this.extension = extension;
    }


    public static Picture createPicture(final int id, final String extension) {
        return new Picture(id, extension);
    }

}
