package io.costax.rabbitmq.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;

@Service
public class FixedRateConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(FixedRateConsumer.class);

    @RabbitListener(concurrency = "3-5", queues = "q.example2.fixedrate")
    public void listen(String message) {
        LOGGER.info("Consuming {} on thread {}", message, Thread.currentThread().getName());

        executeSomeVeryLongAndHighTask();
    }


    private void executeSomeVeryLongAndHighTask() {

        try {
            Thread.sleep(ThreadLocalRandom.current().nextLong(5000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
