# Rabbit-MQ


## Introduction RabbitMQ

- Open source, but also available in enterprise
    - RabbitMQ is an open source messaging system, which means we don't have to pay to use it.
 
    - however, if you need better support, RabbietMQ also provides commercial services.

- Queueing data in real time
    - As a messaging system, RabbitMQ is capable to queuing data in real time to be processed later.

- Reliable & provide high availability
    - Multiple RabbitMQ instances can be deployed in cluster to provide high availability.
    
- Available on-premise or [cloud](https://www.cloudamqp.com/)   
    - If you don't want to maintain your own RabbitMQ server, you can also use RabbitMQ on the cloud.
    - [cloudamqp.com](https://www.cloudamqp.com/) provides RabbitMQ as a service that you can use in no time.


## RabbitMQ vs The Other Player

- RabbitMQ is not the only player in messaging system. However it has several benefits compared to other players.


- Provide web interface for Management and Monitoring.
    - Came with free web interface for Management and Monitoring.
    - We can monitor thing like server status, unprocessed, or processed messages.
    
- Built-in user access control
    - RabbitMQ has build in user access control, from  web interface, we can add user, set username, password, and access level for each user.
    
- RabbitMQ come with built-in REST API
    - Rabbit also come with built-in REST API.
    - This REST API is primarily intended for diagnostic propose but can be used for low volume messaging.
    - However it is quite slow, so it is not recommended to use the REST API to publish messages.
    
- Multiple programming language for client
    - RabbitMQ itself is a server, but it support multiple programming languages for client implementation.
    - Those client includes widely-used languages like: java, C#, Go, Javascript, Python and PHP.
    
---

## Download RabbitMQ

**Tools Required for Next Lecture**

To install RabbitMQ, you must install Erlang first.

You can download Erlang and RabbitMQ from these sites:

1. [Download Erlang](http://www.erlang.org/downloads)

2. [Download RabbitMQ](https://www.rabbitmq.com/download.html)



NOTE: Not all of Erlang-RabbitMQ versions are compatible each other, please see this [compatibility matrix](https://www.rabbitmq.com/which-erlang.html)



Installation Path

During this course, I will use D:/development as my basic installation folder. You can change the basic installation folder as you wish, as long as you have administrative (read/write) access to that folder.

You have to use administrator access for installation and running



Enable RabbitMQ Management Plugin

From RabbitMQ console, type rabbitmq-plugins enable rabbitmq_management


---

## docker images 


The official docker images: **https://hub.docker.com/_/rabbitmq**


```
docker run \
 -d --hostname my-rabbit \
 --name some-rabbit \
 -p 5672:5672 -p 15672:15672 \
 rabbitmq:3.8.2-management-alpine
```

After the  start the image we can see the web dashboard interface: [http://container-ip:15672](http://localhost:15672)


More about the image:

- volume: /var/lib/rabbitmq/mnesia


