package io.costax.rabbitmq.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.costax.rabbitmq.consumer.entity.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.UncheckedIOException;

@Service
public class EmployeeJsonConsumer {

    public static final String Q_EXAMPLE_4_EMPLOYEE = "q.example4.employee";

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeJsonConsumer.class);

    private ObjectMapper objectMapper = new ObjectMapper();

    @RabbitListener(queues = Q_EXAMPLE_4_EMPLOYEE)
    public void listen(String message) {
        Employee emp = toEmp(message);

        LOGGER.info("Employee is {}", emp);
    }

    private Employee toEmp(final String message) {
        try {
            return objectMapper.readValue(message, Employee.class);
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }
}
