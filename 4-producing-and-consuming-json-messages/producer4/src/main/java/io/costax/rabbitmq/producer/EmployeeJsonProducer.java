package io.costax.rabbitmq.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.costax.rabbitmq.producer.entity.Employee;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UncheckedIOException;

@Service
public class EmployeeJsonProducer {

    public static final String Q_EXAMPLE_4_EMPLOYEE = "q.example4.employee";

    @Autowired
    private RabbitTemplate rabbitTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    public void sendMessage(Employee emp) {
        var json = toJson(emp);

        rabbitTemplate.convertAndSend(Q_EXAMPLE_4_EMPLOYEE, json);
    }

    private String toJson(final Employee emp) {
        try {
            return objectMapper.writeValueAsString(emp);
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }
}
