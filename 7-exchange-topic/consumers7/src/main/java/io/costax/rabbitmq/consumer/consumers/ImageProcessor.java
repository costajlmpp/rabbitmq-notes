package io.costax.rabbitmq.consumer.consumers;

import io.costax.rabbitmq.consumer.entity.Picture;
import io.costax.rabbitmq.consumer.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageProcessor {

    private static final String Q_PICTURE_2_IMAGE = "q.picture2.image";

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageProcessor.class);

    @Autowired
    JsonMapper json;

    @RabbitListener(queues = Q_PICTURE_2_IMAGE)
    public void listen(String message) {
        Picture obj = json.convertTo(message, Picture.class);

        LOGGER.info("###> IMAGE-PROCESSOR System processing is [{}]", obj);
    }
}
