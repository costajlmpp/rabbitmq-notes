package io.costax.rabbitmq.consumer.consumers;

import io.costax.rabbitmq.consumer.entity.Picture;
import io.costax.rabbitmq.consumer.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VectorProcessor {

    public static final String Q_PICTURE_2_VECTOR = "q.picture2.vector";

    private static final Logger LOGGER = LoggerFactory.getLogger(VectorProcessor.class);

    @Autowired
    private JsonMapper jsonMapper;

    @RabbitListener(queues = Q_PICTURE_2_VECTOR)
    public void listen(String message) {
        Picture obj = jsonMapper.convertTo(message, Picture.class);

        LOGGER.info("###> VECTOR-PROCESSOR System processing is [{}]", obj);
    }
}
