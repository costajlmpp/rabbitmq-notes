package io.costax.rabbitmq.producer;

import io.costax.rabbitmq.producer.entity.Picture;
import io.costax.rabbitmq.producer.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PictureInputSystemProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(PictureInputSystemProducer.class);

    private static final String X_PICTURE_2 = "x.picture2";

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private JsonMapper jsonMapper;

    public void sendMessage(Picture picture) {

        String routingKey = buildRotingKey(picture);

        LOGGER.info("Sending the to roting key: [{}] the picture [{}]", routingKey, picture);

        rabbitTemplate.convertAndSend(X_PICTURE_2, routingKey, jsonMapper.toJson(picture));
    }

    /**
     * picture.type + "." + ( (picture.getSize() > 4000) ? "large" : "small"  ) + "." + picture.extension
     */
    private String buildRotingKey(final Picture picture) {

        var sb = new StringBuilder();
        sb.append(picture.getSource());
        sb.append(".");

        if (picture.getSize() > 4000) {
            sb.append("large");
        } else {
            sb.append("small");
        }

        sb.append(".");

        sb.append(picture.getExtension());

        return sb.toString();
    }

}
