# 9 Handle Consumer Exception With DLX (solution 2)

Before We Start Solution Two. 

In next lecture, we need to change some applications properties in the src/main/resources/application.yml file:

```yml
spring:
  rabbitmq:
    listener:
      direct:
        acknowledge-mode: manual
      simple:
        acknowledge-mode: manual
```
        
Also, when choosing which class during consumer modification, the types we need to choice are:

- `org.springframework.amqp.core.Message`
- `com.rabbitmq.client.Channel`

----

### About the code:

As the alternative solution, we can do manual rejection if there is an error, instead of throwing Exception.

1. To do this, we need to change our rabbitmq-consumer Go to src/main/resources/application.yml This is Spring centralized configuration. 
 Copy and paste lines required from previous lecture with title “Before We Start Solution Two” We need to add configuration items for manual acknowledge. **Acknowledge is a mechanism for confirm or reject message that has been processed.**

2. Next, we will modify MyPictureImageConsumer.

  - Instead of using String for parameter, we need to use `org.springframework.amqp.core.Message` and `com.rabbitmq.client.Channel`.

  - The `org.springframework.amqp.core.Message` represents whole rabbitmq message, not just its content.
  - The `com.rabbitmq.client.Channel` is like a tunnel where message runs between Java and RabbitMQ.


3. Since we’re using `org.springframework.amqp.core.Message` class, we need to get it’s body, which actually is our JSON string.

4. Now this is how we do manual rejection: 

  - Instead of throwing Exception, we will change the line that is throwing the Exception to:

     ```java
    channel.basicReject(message.getMessageProperties().getDeliveryTag(), false);
    ```

  - BasicReject needs a delivery tag, which is unique for each message, and can be retrieved from parameter "message". Set the requeue parameter to `false`.

  - The side effect of this manual rejection, we also must do manual acknowledge. That is, telling RabbitMQ that our process has been done. So on the end o the method, we also must call channel.basicAck after our process is done.
  
    ```java
    channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    ```

OK, consumer is done. We can run the producer and the consumer and confirm that the behavior is the same of the previous solution.


NOTES: 

- In RabbitMQ, invalid message also go to dead letter exchange, which is “x.mypicture.dlx”, that broadcast it into the queue “q.mypicture.dlx”.

- Clear the queue.
  - Before we move to another lecture, I will delete queues and exchange used.
  - I will also disable consumer by remark the @Service annotation at consumer