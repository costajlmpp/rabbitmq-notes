package io.costax.rabbitmq.consumer.consumers;

import io.costax.rabbitmq.consumer.entity.Picture;
import io.costax.rabbitmq.consumer.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class MyPictureImageConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyPictureImageConsumer.class);

    private static final String Q_MYPICTURE_IMAGE = "q.mypicture.image";

    @Autowired
    private JsonMapper objectMapper;

    @RabbitListener(queues = Q_MYPICTURE_IMAGE)
    public void listen(
            org.springframework.amqp.core.Message message,
            com.rabbitmq.client.Channel channel) throws IOException {

        var p = objectMapper.convertTo(message.getBody(), Picture.class);

        if (p.getSize() >= 6_500L) {
            LOGGER.warn("The Picture [{}] contain a invalid Size and can not be processed", p);

            //throw new AmqpRejectAndDontRequeueException("Picture size too large : " + p);
            channel.basicReject(message.getMessageProperties().getDeliveryTag(), false);

        }

        LOGGER.info("On image : [{}]", p.toString());

        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
