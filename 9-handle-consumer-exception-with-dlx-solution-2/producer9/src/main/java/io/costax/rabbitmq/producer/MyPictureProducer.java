package io.costax.rabbitmq.producer;

import io.costax.rabbitmq.producer.entity.Picture;
import io.costax.rabbitmq.producer.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MyPictureProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyPictureProducer.class);

    private static final String X_MYPICTURE = "x.mypicture";

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private JsonMapper jsonMapper;

    public void sendMessage(Picture picture) {

        var json = jsonMapper.toJson(picture);

        LOGGER.info("Sending to Exchange [{}] the message [{}]", X_MYPICTURE, picture);
        rabbitTemplate.convertAndSend(X_MYPICTURE, "", json);
    }
}
