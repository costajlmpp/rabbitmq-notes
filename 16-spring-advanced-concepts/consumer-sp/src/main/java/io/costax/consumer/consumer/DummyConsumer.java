package io.costax.consumer.consumer;

import io.costax.rabbitmq.entity.DummyMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class DummyConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(DummyConsumer.class);

    @RabbitListener(
            queues = "q.dummy.hello",
            concurrency = "4",
            // Custom prefetch One Container Factory
            containerFactory = "prefetchOneContainerFactory"
            )
    public void process(DummyMessage dummyMessage) {

        LOGGER.info("--- [{}] Received [{}]", Thread.currentThread().getId(), dummyMessage);

        verySlowProcess();
    }


    public void verySlowProcess() {
        try {
            Thread.sleep(20_000L);
            //Thread.sleep(ThreadLocalRandom.current().nextLong(10_000L, 15_000L));
        } catch (InterruptedException e) {
            throw new IllegalStateException("very Long task");
        }
    }
}
