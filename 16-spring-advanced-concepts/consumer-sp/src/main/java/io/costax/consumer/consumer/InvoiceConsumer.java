package io.costax.consumer.consumer;

import io.costax.rabbitmq.entity.InvoiceCancelledMessage;
import io.costax.rabbitmq.entity.InvoiceCreatedMessage;
import io.costax.rabbitmq.entity.InvoicePaidMessage;
import io.costax.rabbitmq.entity.PaymentCancelStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

@Service
@RabbitListener(queues = "q.invoice.multiple-types", containerFactory = "prefetchOneContainerFactory")
public class InvoiceConsumer {


    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceConsumer.class);

    @RabbitHandler
    public void handleInvoiceCreated(InvoiceCreatedMessage message) {
        LOGGER.info("*** [Created] on handleInvoiceCreated : {}", message);
    }

    @RabbitHandler
    public void handleInvoicePaid(InvoicePaidMessage message) {
        LOGGER.info("*** [Paid] on handleInvoicePaid : {}", message);
    }

    @RabbitHandler(isDefault = true)
    public void handleDefault(Object message) {
        LOGGER.info("*** [Default] on handleDefault : {}", message);
    }

    @RabbitHandler
    @SendTo("x.invoice.cancel/")
    public PaymentCancelStatus handleInvoiceCancelled(InvoiceCancelledMessage message) {
        //var randomStatus = ThreadLocalRandom.current().nextBoolean();

        return new PaymentCancelStatus();
    }
}
