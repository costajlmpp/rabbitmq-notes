package io.costax.rabbitmq.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class InvoiceCreatedMessage {

    private String id;
    private BigDecimal value;

    InvoiceCreatedMessage() {}

    private InvoiceCreatedMessage(final String id, final BigDecimal value) {
        this.id = id;
        this.value = value;
    }

    public static InvoiceCreatedMessage createInvoiceCreatedMessage(final String id, final BigDecimal value) {
        return new InvoiceCreatedMessage(id, value);
    }
}
