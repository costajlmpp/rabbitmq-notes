package io.costax.rabbitmq.entity;

import lombok.Data;

@Data
public class DummyMessage {

    private String id;
    private String name;

    @Deprecated
    DummyMessage() {
    }

    private DummyMessage(final String id, final String name) {
        this.id = id;
        this.name = name;
    }

    public static DummyMessage createDummyMessage(final String id, final String name) {
        return new DummyMessage(id, name);
    }
}
