package io.costax.rabbitmq.entity;

import lombok.Data;

@Data
public class InvoiceCanceledMessage {

    private String xpto;

    public InvoiceCanceledMessage() {}

    private InvoiceCanceledMessage(final String xpto) {
        this.xpto = xpto;
    }

    public static InvoiceCanceledMessage createInvoiceCanceledMessage(final String xpto) {
        return new InvoiceCanceledMessage(xpto);
    }
}
