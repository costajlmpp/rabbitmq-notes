package io.costax.rabbitmq.producer;

import io.costax.rabbitmq.entity.InvoiceCanceledMessage;
import io.costax.rabbitmq.entity.InvoiceCreatedMessage;
import io.costax.rabbitmq.entity.InvoicePaidMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceDataProducer {


    public static final String X_INVOICE = "x.invoice";
    public static final String ANY = "any";

    @Autowired
    private RabbitTemplate rabbitTemplate;


    public void invoiceCreatedMessage(InvoiceCreatedMessage message) {
        rabbitTemplate.convertAndSend(X_INVOICE, ANY, message);
    }

    public void invoicePaidMessage(InvoicePaidMessage message) {
        rabbitTemplate.convertAndSend(X_INVOICE, ANY, message);
    }

    public void invoiceCanceledMessage(InvoiceCanceledMessage message) {
        rabbitTemplate.convertAndSend(X_INVOICE, ANY, message);
    }

}
