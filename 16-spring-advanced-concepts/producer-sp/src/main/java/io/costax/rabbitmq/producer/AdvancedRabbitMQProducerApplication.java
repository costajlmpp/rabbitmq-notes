package io.costax.rabbitmq.producer;

import io.costax.rabbitmq.entity.InvoiceCanceledMessage;
import io.costax.rabbitmq.entity.InvoiceCreatedMessage;
import io.costax.rabbitmq.entity.InvoicePaidMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.math.BigDecimal;

@SpringBootApplication
@EnableScheduling
public class AdvancedRabbitMQProducerApplication implements CommandLineRunner {


    // Note: we need to create the queues, exchange and the bindings manually the rabbitMQ server
    // check the About.md file

    @Autowired
    DummyProducer producer;

    @Autowired
    InvoiceDataProducer invoiceDataProducer;

    public static void main(String[] args) {
        SpringApplication.run(AdvancedRabbitMQProducerApplication.class, args);
    }

    @Override
    public void run(final String... args) {

        //for (int i = 0; i < 750; i++) {
        //    producer.sendDummy(DummyMessage.createDummyMessage("X-" + i, "Duke is the greatest hero of the universe!!!"));
        //}

        invoiceDataProducer.invoiceCreatedMessage(InvoiceCreatedMessage.createInvoiceCreatedMessage("Invoice-1", BigDecimal.TEN));
        invoiceDataProducer.invoicePaidMessage(InvoicePaidMessage.createInvoicePaidMessage("Invoice-1", BigDecimal.TEN, "Java Planet"));
        invoiceDataProducer.invoiceCanceledMessage(InvoiceCanceledMessage.createInvoiceCanceledMessage("Invoice-1"));
    }

}
