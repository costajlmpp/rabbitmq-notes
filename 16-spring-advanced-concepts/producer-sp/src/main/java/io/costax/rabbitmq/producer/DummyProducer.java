package io.costax.rabbitmq.producer;

import io.costax.rabbitmq.entity.DummyMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class DummyProducer {

    private final RabbitTemplate rabbitTemplate;

    public DummyProducer(final RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendDummy(DummyMessage message) {
        rabbitTemplate.convertAndSend("x.dummy", "hello", message);
    }
}
