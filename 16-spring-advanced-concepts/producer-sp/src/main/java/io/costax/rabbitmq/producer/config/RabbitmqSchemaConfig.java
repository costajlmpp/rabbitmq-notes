package io.costax.rabbitmq.producer.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitmqSchemaConfig {

    @Bean
    public Declarables createRabbitmqSchema() {

        final DirectExchange invoicesExchange = new DirectExchange("x.invoice");
        final Queue multipleTypeQueue = new Queue("q.invoice.multiple-types", true);
        final Binding multipleTypeQueueBinder = BindingBuilder.bind(multipleTypeQueue)
                .to(invoicesExchange)
                .with("any");

        final FanoutExchange fanoutExchange = new FanoutExchange("x.invoice.cancel");
        final Queue qInvoiceCancel = new Queue("q.invoice.cancel");
        final Binding qInvoiceCancelBinder = BindingBuilder.bind(qInvoiceCancel).to(fanoutExchange);





        final DirectExchange directExchangeDummy = new DirectExchange("x.dummy");
        final Queue queueDummy = new Queue("q.dummy.hello", true);
        final Binding dummyBinding = BindingBuilder.bind(queueDummy)
                .to(directExchangeDummy)
                .with("hello");


        return new Declarables(

                invoicesExchange, multipleTypeQueue, multipleTypeQueueBinder,

                fanoutExchange, qInvoiceCancel, qInvoiceCancelBinder,

                directExchangeDummy, queueDummy, dummyBinding,


                new FanoutExchange("x.another-dummy", true, false, null),
                new Queue("q.another-dummy"),
                new Binding("q.another-dummy", Binding.DestinationType.QUEUE, "x.another-dummy", "", null)
        );
    }

}
