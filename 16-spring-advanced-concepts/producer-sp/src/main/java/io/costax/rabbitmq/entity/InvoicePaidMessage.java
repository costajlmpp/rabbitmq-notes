package io.costax.rabbitmq.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class InvoicePaidMessage {

    private String id;
    private BigDecimal value;
    private String local;

    public InvoicePaidMessage() {
    }

    private InvoicePaidMessage(final String id, final BigDecimal value, final String local) {
        this.id = id;
        this.value = value;
        this.local = local;
    }

    public static InvoicePaidMessage createInvoicePaidMessage(final String id, final BigDecimal value, final String local) {
        return new InvoicePaidMessage(id, value, local);
    }
}
