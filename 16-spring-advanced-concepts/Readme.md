# JSON Message Converter

So far to send and process messages, We convert the object into JSON string using Jackson object mapper, then call convertAndSend from rabbitTemplate, passing that JSON String. So implicitly, **we have something who convert the message for us**. This is called as “Message Converter”.

RabbitMQ will receive and store message into particular format, so this message converter job is converting that string into RabbitMQ message format, or vice versa. Since we are using Spring Boot, Spring provides `SimpleMessageConverter` class as default converter.


- This class will handles conversion of string data type. Since the default converter requires String as data type, we convert the object (which is MyMessage data type) into String.

- It does not have to be in JSON format, but using JSON as format will give us better portability, since JSON is already popular notation for message. Means any consumer from any programming language will be easily convert from-or-to-JSON. Since JSON is popular format, Spring already provides JSON message conveter for us, which also based on Jackson library.

For this examples, we will use this JSON converter, which is class Jackson2JsonMessageConverter.


# create our own RabbitTemplate

- we will create our own RabbitTemplate to be used during programming. To do this, we must inject a bean with type RabbitTemplate. So create this method, and annotate the method as @Bean to tells Spring that we will use this instance of RabbitTemplate for producer project.


For connectionFactory, import the interface
Inside the method, we will create our own rabbit template, and set the converter for that template using Jackson2JsonMessageConverter, like this For the `Jackson2JsonConverter`, use this method

See the `RabbitmqConfig.java` file, and execute the producer to see the message in the rabbitMQ server.

  - The payload, or the message body, is already on JSON format without we do manual conversion using ObjectMapper.
  - Also note that on message header, we have `application/json` as content type.
  - And this one the `__TypeId__` header.
    - Spring add this `__TypeId__` header with value is the full name of entity class including package. So in this case, it will add DummyMessage which currently under package `io.costax ... .DummyMessage`

```
Properties
priority: 0
delivery_mode:2
headers:
    
    __TypeId__:    DummyMessage
    
    content_encoding:   UTF-8

    content_type:   application/json

Payload
    {"id":"123","name":"Duke is the greatest hero of the universe!!!"}
```


## If You Can't Control The Package Name

If you works with other system that already publish to RabbitMQ, chances are you cannot control the `__TypeId__` header (which is Spring specific).

Maybe other publisher use different header name, like type.
If it different header name exists (for example, type), there are two approach:

1. Using if-else in @RabbitListener, based on existing header. See sample code.

    ```java
    /**
     * Sample Listener if you don't have __TypeID__ but have other header (for example : type)
     */
    public class ListenerSample {
    
        // Adjust the header name if required, on @Header parameter
        @RabbitListener(queues = "q.finance.invoice")
        public void listenInvoiceCreated(
                    @Payload String message, 
                    @Header(AmqpHeaders.DELIVERY_TAG) long tag,
                    @Header("type") String type) 
                throws IOException {
            
            if (StringUtils.equalsIgnoreCase(type, "invoice.paid")) {
                log.info("Delegate to invoice paid handler");
            } else if (StringUtils.equalsIgnoreCase(type, "invoice.created")) {
                log.info("Delegate to invoice created handler");
            } else {
                log.info("Delegate to default handler");
            }
        }
    }
    ```

2. Cleaner approach requires a little bit more code. See code for RabbitMqConfig.java. Using this approach, you can write listener like we seen on the lecture (see sample listener)

    ```java
    import org.springframework.amqp.core.Message;
    import org.springframework.amqp.core.MessagePostProcessor;
    import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
    import org.springframework.amqp.rabbit.connection.ConnectionFactory;
    import org.springframework.amqp.rabbit.core.RabbitTemplate;
    import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
    import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
    import org.springframework.context.annotation.Bean;
    import org.springframework.context.annotation.Configuration;
    
    import com.course.finance.message.invoice.InvoiceCreatedMessage;
    import com.course.finance.message.invoice.InvoicePaidMessage;
    
    @Configuration
    public class RabbitmqConfig {
    
        @Bean(name = "rabbitListenerContainerFactory")
        public SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory(
                SimpleRabbitListenerContainerFactoryConfigurer configurer, ConnectionFactory connectionFactory) {
            SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
            configurer.configure(factory, connectionFactory);
    
            factory.setAfterReceivePostProcessors(new MessagePostProcessor() {
    
                @Override
                public Message postProcessMessage(Message message) throws AmqpException {
                    var type = message.getMessageProperties().getHeaders().get("type").toString();
                    String typeId = null;
    
                    if (StringUtils.equalsIgnoreCase(type, "invoice.paid")) {
                        typeId = InvoicePaidMessage.class.getName();
                    } else if (StringUtils.equalsIgnoreCase(type, "invoice.created")) {
                        typeId = InvoiceCreatedMessage.class.getName();
                    }
    
                    Optional.ofNullable(typeId).ifPresent(t -> message.getMessageProperties().setHeader("__TypeId__", t));
    
                    return message;
                }
    
            });
    
            return factory;
        }
    
        @Bean
        Jackson2JsonMessageConverter jsonMessageConverter() {
            return new Jackson2JsonMessageConverter();
        }
    
        @Bean
        RabbitTemplate rabbitTemplate(Jackson2JsonMessageConverter converter, ConnectionFactory connectionFactory) {
            RabbitTemplate template = new RabbitTemplate(connectionFactory);
            template.setMessageConverter(new Jackson2JsonMessageConverter());
            return template;
        }
    
    }
    ```



If other publisher does not has header to indicate the type, then you must find the workaround, but basically you can put the workaround algorithm on one of the places above.

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import com.course.finance.message.invoice.InvoiceCreatedMessage;
import com.course.finance.message.invoice.InvoicePaidMessage;

@Service
@RabbitListener(queues = "q.invoice")
public class InvoiceListener {

    private static final Logger log = LoggerFactory.getLogger(InvoiceListener.class);

    @RabbitHandler
    public void listenInvoiceCreated(InvoiceCreatedMessage message) {
        log.info("Listening invoice created : {}", message);
    }

    @RabbitHandler
    public void listenInvoicePaid(InvoicePaidMessage message) {
        log.info("Listening invoice paid : {}", message);
    }

    @RabbitHandler(isDefault = true)
    public void listenDefault(Message message) {
        log.info("Default invoice listener : {}", message.getMessageProperties().getHeaders());
    }

}
```



## Scheduling Consumer

