package io.costax.consumerdirectxspringbootretrymechanism.consumer;

import io.costax.consumerdirectxspringbootretrymechanism.entity.EmployeeData;
import io.costax.consumerdirectxspringbootretrymechanism.entity.Picture;
import io.costax.consumerdirectxspringbootretrymechanism.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SpringEmployeeDataConsumer {

    @Autowired
    private JsonMapper objectMapper;

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringEmployeeDataConsumer.class);

    @RabbitListener(queues = "q.spring2.accounting.work")
    public void listenImage(String message) throws IOException {
        var employeeData = objectMapper.convertTo(message, EmployeeData.class);
        LOGGER.info("[Accounting] Consuming  employee-Data : {}", employeeData);

        if ("Invalid".equalsIgnoreCase(employeeData.getName())) {
            throw new IOException("Employee " + employeeData + " size too large : " + employeeData);
        }

        LOGGER.info("[Accounting] Processing  : {}", employeeData);
    }

    @RabbitListener(queues = "q.spring2.marketing.dead")
    public void listenVector(String message) {
        var employeeData = objectMapper.convertTo(message, EmployeeData.class);

        LOGGER.info("[Marketing] Consuming  : {}", employeeData);
        LOGGER.info("[Marketing] Processing  : {}", employeeData);
    }

}
