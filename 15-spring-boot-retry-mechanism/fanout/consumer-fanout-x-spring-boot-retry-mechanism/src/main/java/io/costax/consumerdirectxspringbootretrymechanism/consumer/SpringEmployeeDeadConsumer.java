package io.costax.consumerdirectxspringbootretrymechanism.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SpringEmployeeDeadConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringEmployeeDeadConsumer.class);
    
    public static final String Q_SPRING_2_ACCOUNTING_DEAD = "q.spring2.accounting.dead";
    public static final String Q_SPRING_2_MARKETING_DEAD = "q.spring2.marketing.dead";

    @RabbitListener(queues = Q_SPRING_2_ACCOUNTING_DEAD)
    public void listenImage(String message) throws IOException {
        LOGGER.info("[DEAD ACCOUNTING] Send some notification for the Administrator : {}", message);
    }

    @RabbitListener(queues = Q_SPRING_2_MARKETING_DEAD)
    public void listenVector(String message) throws IOException {
        LOGGER.info("[DEAD MARKETING] Send some notification for the Administrator : {}", message);
    }
}
