# Fanout exchange schema, Spring boot retry mechanism

![fanout exchange schema](spring-retry-mechanism-for-fanout-exchage.png)

## Resources DEAD:

### Exchanges: 

1. `x.spring2.dead`
    - type: `direct`
    
    
### Queues:

1. `q.spring2.accounting.dead`
2. `q.spring2.marketing.dead`

### Bindings:

- Exchange: `x.spring2.dead`
- To Queue: `q.spring2.accounting.dead`
- Routing key: `dead-accounting`


- Exchange: `x.spring2.dead`
- To Queue: `q.spring2.marketing.dead`
- Routing key: `dead-marketing`


## Resources Work:

1. `x.spring2.work`
    - type: `fanout`
    
### Queues:

1. `q.spring2.accounting.work`
    - Dead Letter Exchange: `x.spring2.dead`
    - Dead letter routing key: `dead-accounting`

2. `q.spring2.marketing.work`
    - Dead Letter Exchange: `x.spring2.dead`
    - Dead letter routing key: `dead-marketing`
    
### Bindings:

- Exchange: `x.spring2.work`
- To Queue: `q.spring2.accounting.work`


- Exchange: `x.spring2.work`
- To Queue: `q.spring2.marketing.work`