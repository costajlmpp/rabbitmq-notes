package io.costax.rabbitmq.producer;

import io.costax.rabbitmq.producer.entity.Employee;
import io.costax.rabbitmq.producer.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpringEmployeeProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringEmployeeProducer.class);

    private static final String X_SPRING2_WORK = "x.spring2.work";

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private JsonMapper jsonMapper;

    public void sendMessage(Employee e) {

        var json = jsonMapper.toJson(e);

        LOGGER.info("Sending to Exchange [{}] the message [{}]", X_SPRING2_WORK, e);
        rabbitTemplate.convertAndSend(X_SPRING2_WORK, "", json);
    }
}
