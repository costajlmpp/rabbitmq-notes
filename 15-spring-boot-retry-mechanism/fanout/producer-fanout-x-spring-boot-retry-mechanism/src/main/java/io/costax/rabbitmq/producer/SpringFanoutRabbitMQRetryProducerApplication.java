package io.costax.rabbitmq.producer;

import io.costax.rabbitmq.producer.entity.Employee;
import io.costax.rabbitmq.producer.entity.Picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.time.LocalDate;

import static io.costax.rabbitmq.producer.entity.Picture.PNG;

@SpringBootApplication
@EnableScheduling
public class SpringFanoutRabbitMQRetryProducerApplication implements CommandLineRunner {

    // Note: we need to create the queues, exchange and the bindings manually the rabbitMQ server
    // check the About.md file

    //@Autowired
    //PictureInputSystemProducer producer;

    @Autowired
    SpringEmployeeProducer producer;

    public static void main(String[] args) {
        SpringApplication.run(SpringFanoutRabbitMQRetryProducerApplication.class, args);
    }

    @Override
    public void run(final String... args) {
        producer.sendMessage(new Employee("emp-without-name", "", LocalDate.now()));
    }

}
