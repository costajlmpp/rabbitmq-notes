package io.costax.consumerdirectxspringbootretrymechanism;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerDirectXSpringBootRetryMechanismApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerDirectXSpringBootRetryMechanismApplication.class, args);
    }

}
