# Direct exchange schema, Spring boot retry mechanism

![Direct exchange schema](spring-retry-mechanism-for-direct-exchage.png)

About:
  - Notice that we don’t have wait exchange for this schema.
  - Spring will handle the wait and retry for us.
  - Also in this sample, I will create only one consumer class with two methods, one for image and one for vector.
  - This is not mandatory, I just want to show that this is possible.
  - If you want to create two different consumer class, it will be fine too.

In this example i'm going to create the queues manually, to a better understanding.

1. create the work exchange.

    - It is a direct exchange with name `x.spring.work`

2. create two work queues: 

    - `q.spring.image.work`
    - `q.spring.vector.work`
    - The dead letter exchange for both queues is: `x.spring.dead`
    - We will create the dead letter exchange later.

3. Create the queue `q.spring.image.work` with dead letter exchange to `x.spring.dead`
4. Create the queue `q.spring.vector.work` with dead letter exchange to `x.spring.dead`

5. Now, bind the work exchange with the queues:

    - For `q.spring.image.work` queue, it will have two bindings:
        - routing key `jpg`
        - routing key `png`

    - For `q.spring.vector.work` queue, it will have one binding: 
        - routing key `svg`

6. Now let’s create the dead letter exchange.
    
    - It is a `direct` exchange with name `x.spring.dead`

7. create two queues: 
    - `q.spring.image.dead` 
    - `q.spring.vector.dead`

8. Now, bind the dead exchange with the queues:

    - For `q.spring.image.dead` queue, it will have two bindings:
        - routing key `jpg` 
        - routing key `png`

    - For `q.spring.vector.dead` queue, it will have one binding with routing key `svg`.



---

```yml
spring:
  rabbitmq:
    host: localhost
    port: 5672
    virtual-host: jcosta
    username: jcosta
    password: jcosta
    listener:
      simple:
        retry:
          enabled: on
          initial-interval: 3s
          max-interval: 10s
          max-attempts: 5
          multiplier: 2
```

- `max-attempts`: We have 5 attempts of consuming images.

- `initial-interval`: The interval between first consume and second consume is 3 seconds, because we define the “initial-interval” on application.yml as 3 seconds.

- `multiplier`: The interval between second and third consume is 6 seconds, because we define the multiplier on application.yml as 2. This means, interval (x+1) is twice of interval (x)

- `max-interval`: The interval between third and fourth consume is 10 seconds. Notice that we don’t get 12 seconds, which is multiplication of multiplier and 6 seconds. Instead, we get 10 seconds. This happened because we define “max-interval” at application.yml as 10 seconds.

So spring will manage the interval not to exceed this maximum value, which is currently ten seconds.
So the interval between fourth and fifth consume also ten seconds, which is max interval.
After fifth consume, we still get error, so spring throw error and send the message todead letter exchange.
This is all done by spring.