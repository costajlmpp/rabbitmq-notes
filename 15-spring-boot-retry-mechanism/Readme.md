# Spring boot retry mechanism

Actually, Spring boot already provides retry mechanism, and it is quite powerful.

Using it, we don’t need to write a code for processing error handler.
We only need to configure **application.yml** to use it.
It has several features, where we can define the **interval for retry**, we can also define **maximum number of retry** and we can also define **multiplier for retry interval**, That is, we can define different interval for attempt x and attempt (x+1), based on this multiplier.


## Considerations About Spring retry mechanism

Spring retry mechanism needs less code compared to previous retry mechanism.

However it come at a cost.
Spring retry mechanism will block on a message that has error.

```

--> X. -> Queue (with messages: A, B, C) 

```

See this illustration: You have message A, B, C (come in that order)

  - Message A has error during process and the error handler is using Spring retry mechanism. Let's just say for 3 times, with 1 minute retry interval.

 - Spring will try to process message A, and will not process B and C until message A is successfully processed, or the retry period is over. In this case, if message A has permanent error (e.g. file size is too large), it will block the process for 3 minutes (which is 3 times, 1 minute each). In other words, message B must wait for 3 minutes to be processed.

 - So consider whether Spring retry is suitable for your needs. If your exception mostly a technical exception (e.g. network exception) and you can afford some delay taken by retrying, then Spring retry should be fine.
